FROM golang:1-alpine

RUN apk add --no-cache git
RUN go get -u github.com/cloudflare/cloudflare-go/...

ADD ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

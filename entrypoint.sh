#!/bin/sh

[[ -f /env.sh ]] && . /env.sh

DOMAIN=$1
shift
CURRENT_IP=$1
shift

for host in "$@"; do
  /go/bin/flarectl dns o \
    --zone ${DOMAIN} --name ${host} --type A --content ${CURRENT_IP}
done
